# K8s Platform Documentation

## Что сделано:

1. **Подготовлены конфиги для развёртывания K8s в GKE через Terraform, развёрнут кластер**
  
    Сделал gitlab-ci пайплайн для развёртывания / модификации k8s в GKE:
    - https://gitlab.com/ujhgj-k8s-platform/k8s-platform-infrastructure/-/pipelines
    - https://gitlab.com/ujhgj-k8s-platform/k8s-platform-infrastructure/-/blob/master/.gitlab-ci.yml
    
    Больше информации в Readme в репозитории https://gitlab.com/ujhgj-k8s-platform/k8s-platform-infrastructure.
    
    **ToDo:**
    - готовить контекст для kubectl автоматически, опираясь на terraform output: https://github.com/terraform-google-modules/terraform-google-kubernetes-engine/issues/393
    - устанавливать нужный для Gitlab CI сервисный аккаунт и сервисы платформы из следующего пункта автоматически
    
2. **Подготовлен Helmfile для развёртывания инфраструктурных сервисов**

    См. репозиторий https://gitlab.com/ujhgj-k8s-platform/k8s-platform-services.
    
    На данный момент содержит манифесты для развёртывания:
    - nginx ingress controller
    - cert manager
    - cluster certificate issuer для cert manager
    
    которые используются в работе приложений, а так же развёрнут 
    - prometheus stack: 
    
        - https://grafana.35.198.151.123.nip.io/
        - https://prometheus.35.198.151.123.nip.io/
        - https://alertmanager.35.198.151.123.nip.io/
          
        (логин: `admin`, пароль: `promo-Prom!`)
    
    **Проблемы:**
    - ~~при установке чартов с нуля создание cluster certificate issuer валится из-за неполностью готового cert manager'а; увы, директивы wait: true и needs, как и вынос в отдельный helmfile не помогают, помогает только подождать~~
    - почему-то не скрапятся метрики с CoreDNS

    **ToDo:**
    - создание секрета для аутентификации в prometheus и alertmanager вынести в helmfile (пока что создан руками)
    - сервис-монитор для hipster-shop
    - развёртывание и использование EFK, Jaeger, Istio
    - пайплайн (сейчас .gitlab-ci.yml в репозитории нерабочий)
    
3. **Склонировал и доработал GoogleCloudPlatform/microservices-demo**

    См. репозиторий https://gitlab.com/ujhgj-k8s-platform/hipster-shop.

    Сделал чарт с настраиваемым ингрессом для приложения. Приложение обрезал, чтобы влезало несколько инстансов приложения в триальный кластер GKE, чтобы обкатать пайплан с ревью.
    
    Добавил пайплайн (https://gitlab.com/ujhgj-k8s-platform/hipster-shop/-/pipelines), который 
    - позволяет деплоить приложение в отдельный namespace из ветки,
    - деплоит приложение в отдельный namespace, если создан merge-request и деплоит при каждом новом коммите в этот merge-request,
    - если в ветку не добавляются коммиты 1 день - environment (в терминах Gitlab) останавливается,
    - деплоит приложение в прод при мерже merge-request'а.
    
    Вся функциональность - коробочная интеграции Gitlab CI + K8s, контроллер не делал. 
    
    Приложение доступно по адресу: https://shop.35.198.151.123.nip.io/
    
    **ToDo:**
    - добавить сборку и тестирование docker-образов
    - автоматически удалять остановленный environment (в терминах Gitlab) и созданный для него namespace
    - добавить JS код для тулбара ревью прямо в приложении 
    - опробовать и описать в документации процесс разработки со scaffold (а-ля https://gitlab.com/ujhgj-k8s-platform/hipster-shop/-/blob/master/docs/development-guide.md)
